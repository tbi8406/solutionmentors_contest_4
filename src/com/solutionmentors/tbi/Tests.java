package com.solutionmentors.tbi;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class Tests {

    @Test
    public void fromTask() {
        final String input = "3 " +
                "4 15 " +
                "1 3 10 11 " +
                "1 2 7 " +
                "4 100 " +
                "1 5 10 50 " +
                "3 14 12 " +
                "2 1000000000 " +
                "1 1 " +
                "1";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        Main.main(null);
        String actual = output.toString(StandardCharsets.UTF_8);
        assertEquals("6\r\n13\r\n1000000000\r\n", actual);
    }

    @Test
    public void withoutPromotion() {
        final String input = "2 " +
                "3 12 " +
                "1 2 3 " +
                "1 5 " +
                "3 7 " +
                "1 2 1 " +
                "1 1 ";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        Main.main(null);
        String actual = output.toString(StandardCharsets.UTF_8);
        assertEquals("8\r\n6\r\n", actual);
    }
}
