package com.solutionmentors.tbi;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int casesCount = scanner.nextInt();
        for (int cc = casesCount; cc > 0; cc--) {
            int n = scanner.nextInt();
            int c = scanner.nextInt();
            int[] a = new int[n]; //positions
            int[] b = new int[n]; //salaries
            for (int i = 0; i < n; i++) {
                a[i] = scanner.nextInt();
            }
            for (int i = 0; i < n - 1; i++) {
                b[i] = scanner.nextInt();
            }
            int[] money = new int[n]; //money on a given day by positions
            money[0] = a[0];
            for (int position = 1; position < n; position++) {
                money[position] = -1; // -1 indicates that the position has not been reached
            }
            calculation:
            for (int day = 1; ; day++) {
                for (int position = n - 1; position >= 0; position--) {
                    if (money[position] >= c) {
                        System.out.println(day);
                        break calculation;
                    }
                    if (money[position] != -1) {
                        if (position < n - 1 && money[position] >= b[position] && money[position + 1] < money[position] - b[position]) {
                            money[position + 1] = money[position] - b[position];
                        }
                        money[position] += a[position];
                    }
                }
            }

        }
    }

}
